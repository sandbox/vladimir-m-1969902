<?php
/**
 * @file
 * Main file for PaiApache class.
 */

// Include pai_commands.inc file.
module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Class PaiApache
 */
class PaiApache {
  var $path;
  var $command;
  var $as_root;
  var $user;
  var $pass;

  /**
   * Constructor.
   */
  function __construct() {
    $this->path =
    $this->command =
    $this->as_root =
    $this->user =
    $this->password = NULL;
  }

  /**
   * Php information.
   */
  function phpInfo() {
    $command = "php -i";
    return $result = pai_execute_command($command);
  }

  /**
   * Restart apache server.
   */
  function restartApache() {
    // @todo - Features.
    if ($this->as_root == 0) {
      if (empty($this->path)) {
        $command = $this->command;
      }
      else {
        $command = "sh -c \"cd $this->path && $this->command\"";
      }
    }
    else {
      if (empty($this->path)) {
        $command = "echo $this->password | sudo -S sudo -u $this->user sh -c '$this->command'";
      }
      else {
        $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'cd $this->path && $this->command'";
      }
    }
    //watchdog('PaiApache command', $command);
    return pai_execute_command($command);
  }
}
