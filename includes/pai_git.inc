<?php
/**
 * @file
 * Manage branches/tags for pai projects.
 */

/**
 * Class PaiGit
 */
class PaiGit {
  var $docroot;
  var $point; // Point its name of tag or branch.

  /**
   * Constructor.
   */
  function __construct() {
    $this->docroot = NULL;
    $this->point = NULL;
  }

  /**
   * Get GIT tags.
   */
  function getTags() {
    return pai_execute_command($command = "cd $this->docroot && git tag -l");
  }

  /**
   * Get Git branches.
   */
  function getBranches() {
    return pai_execute_command($command = "cd $this->docroot && git branch ");
  }

  /**
   * Git checkout.
   */
  function checkout() {
    return pai_execute_command($command = "cd $this->docroot && git checkout $this->point");
  }

  /**
   * Git log.
   */
  function logs() {
    return pai_execute_command($command = "cd $this->docroot && git log");
  }

  /**
   * Git pull.
   */
  function pull() {
    return pai_execute_command($command = "cd $this->docroot && git pull");
  }
}
