<?php
/**
 * @file
 * Main file for PaiDatabase class.
 */

// Include pai_commands.inc file.
module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Class PaiDatabase
 */
class PaiDatabase {
  // Define variables.
  var $db_host;
  var $db_port;
  var $db_root;
  var $db_root_pass;
  var $db_name;
  var $db_user;
  var $db_user_pass;
  var $db_file;

  /**
   * Define constructor function.
   */
  function __construct() {
    $this->db_host =
    $this->db_port =
    $this->db_root =
    $this->db_root_pass =
    $this->db_name =
    $this->db_user =
    $this->db_user_pass =
    $this->db_file = NULL;
    $this->db_user_host = 'localhost';
  }

  // Create database.
  function databaseExist() {
    $command = "mysql -u $this->db_root -h $this->db_host -p$this->db_root_pass -P$this->db_port -e \"SELECT schema_name FROM information_schema.schemata WHERE schema_name = '$this->db_name';\"";
    return pai_execute_command($command);
  }
  // Create database.
  function createDatabase() {
    $command = "mysql -u $this->db_root -h $this->db_host -p$this->db_root_pass -P$this->db_port -e \"CREATE DATABASE $this->db_name;\"";
    return pai_execute_command($command);
  }

  // Remove database.
  function dropDatabase() {
    $command = "mysql -u $this->db_root -h $this->db_host -p$this->db_root_pass -P$this->db_port -e \"DROP DATABASE $this->db_name;\"";
    return pai_execute_command($command);
  }

  // Create user.
  function createUser() {
    $command = "mysql -u $this->db_root -h $this->db_host -p$this->db_root_pass -P$this->db_port -e \"CREATE USER '$this->db_user'@'$this->db_user_host' IDENTIFIED BY '$this->db_user_pass';\"";
    return pai_execute_command($command);
  }

  // Remove user.
  function dropUser() {
    $command = "mysql -u $this->db_root -h $this->db_host -p$this->db_root_pass -P$this->db_port -e \"DROP USER '$this->db_user'@'$this->db_user_host';\"";
    return pai_execute_command($command);
  }

  // Restore database from file.
  function restoreDatabse() {
    $command = "mysql -u $this->db_root -h $this->db_host -p$this->db_root_pass $this->db_name -P$this->db_port < $this->db_file";
    return pai_execute_command($command);
  }

  // Grant user access to database.
  function grantAccess() {
    $command = "mysql -u $this->db_root -h $this->db_host -p$this->db_root_pass -P$this->db_port -e \"GRANT ALL PRIVILEGES ON $this->db_name.* TO '$this->db_user'@'$this->db_user_host';\"";
    pai_execute_command($command);
    $command = "mysql -u $this->db_root -h $this->db_host -p$this->db_root_pass -P$this->db_port -e \"FLUSH PRIVILEGES;\"";
    return pai_execute_command($command);
  }

  // MySql stop/start.
  function mysqlServer($op) {
    // @todo - Features.
    return;
  }
}
