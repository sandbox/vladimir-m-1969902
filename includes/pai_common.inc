<?php
/**
 * @file
 * Main file for pai_commands includes.
 */

// Include file to decrypt password.
module_load_include('module', 'password_field', 'password_field');

/**
 * Helper function for commands execution.
 */
function pai_execute_command($command) {
  $descriptors = array(
    // Stdin - Read channel.
    0 => array("pipe", "r"),
    // Stdout - Write channel.
    1 => array("pipe", "w"),
    // Stdout - Error channel.
    2 => array("pipe", "w"),
    // Stdin - This is the pipe we can feed the password into.
    3 => array("pipe", "r"),
  );

  $process = proc_open($command, $descriptors, $pipes);

  if (!is_resource($process)) {
    watchdog('pai_execute_command', "Can't open resource with proc_open.");
    return;
  }

  // Nothing to push to input.
  fclose($pipes[0]);

  $output = stream_get_contents($pipes[1]);
  fclose($pipes[1]);

  $error = stream_get_contents($pipes[2]);
  fclose($pipes[2]);

  // @todo: Write passphrase in pipes[3].
  fclose($pipes[3]);

  // Close all pipes before proc_close!
  $code = proc_close($process);

  return array($output, $error, $code);
}

/**
 * Convert strings to machine name.
 */
function pai_string_to_machinename($text) {
  static $texts;

  if (empty($text) || !is_string($text)) {
    return '';
  }

  if (!is_array($texts)) {
    $texts = array();
  }

  if (!isset($texts[$text])) {
    $texts[$text] = preg_replace('/_+/', '_', preg_replace('/[^a-z0-9]/', '_', strtolower($text)));
  }

  return $texts[$text];
}

/**
 * PAI project path.
 */
function pai_paths() {
  $document_root = $_SERVER['DOCUMENT_ROOT'];

  $pai_paths = array(
    'docroot' => $document_root,
    'pai' => $document_root . drupal_get_path('module', 'pai'),
    'modules' => $document_root . drupal_get_path('module', 'pai') . '/modules',
    'sites' => $document_root . drupal_get_path('module', 'pai') . '/sites',
  );

  return $pai_paths;
}

/**
 * Check if node exist.
 */
function pai_node_exist($nid) {
  $conditions = array(
    'nid' => $nid,
  );

  return _pai_db_select('node', $conditions);
}

/**
 * Get list of servers, projects.
 */
function pai_nodes_list($type, $uid = NULL) {
  switch ($type) {
    case 'server':
      $conditions['type'] = 'server';
      break;

    case 'project':
      $conditions['type'] = 'project';
      break;
  }

  if ($uid) {
    $conditions['uid'] = $uid;
  }

  return _pai_db_select('node', $conditions);
}

/**
 * Helper function to select data from database using defined conditions.
 */
function _pai_db_select($table, $conditions = array()) {
  $result = NULL;

  if (!empty($table)) {
    $query = db_select($table, 't');
    $query->fields('t');
    if (!empty($conditions)) {
      foreach ($conditions as $field => $value) {
        $query->condition($field, $value, '=');
      }
    }
    $result = $query->execute()->fetchAll();
  }

  return $result;
}

/**
 * List nodes by condition.
 */
function _pai_list_nodes($type, $operations = array()) {
  global $user;

  $header = array(
    'title' => array(
      'data' => t('Title'),
      'field' => 'n.title',
    ),
    'author' => t('Author'),
    'status' => array(
      'data' => t('Status'),
      'field' => 'n.status',
    ),
    'changed' => array(
      'data' => t('Updated'),
      'field' => 'n.changed',
      'sort' => 'desc',
    ),
    'option' => array(
      'data' => t('Operation'),
    ),
  );

  $select = db_select('node', 'n')
    ->extend('PagerDefault')
    ->extend('TableSort');

  $select->condition('uid', $user->uid)
    ->condition('type', $type, '=')
    ->fields('n', array('nid'))
    ->limit(10)
    ->orderByHeader($header)
    ->orderBy('changed', 'DESC');

  // Execute the query.
  $nids = $select->execute()->fetchCol();
  $nodes = node_load_multiple($nids);

  if (!empty($nodes)) {
    // Build the rows.
    foreach ($nodes as $node) {
      if (empty($operations)) {
        // Set up default options.
        $operations = t('<a href="/node/[nid]/edit">Edit</a>  <a href="/node/[nid]/delete">Delete</a>');
      }
      $ops = str_replace('[nid]', $node->nid, $operations);
      $options[$node->nid] = array(
        'title' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => $node->title,
            '#href' => 'http://' . $node->title,
            '#options' => array('edit', 'delete'),
            '#suffix' => ' ' . theme('mark', array('type' => node_mark($node->nid, $node->changed))),
          ),
        ),
        'author' => theme('username', array('account' => $node)),
        'status' => $node->status ? t('published') : t('not published'),
        'changed' => format_date($node->changed, 'short'),
        'option' => $ops,
      );
    }
  }

  // Build the tableselect.
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => !empty($options) ? $options : NULL,
    '#empty' => t('No content available.'),
  );

  // Build the pager.
  $form['pager'] = array(
    '#markup' => theme('pager'),
  );

  return $form;
}

/**
 * Get information about server.
 */
function pai_common_phpinfo() {
  $result = array();

  $php = new PaiApache();
  $phpinfo = $php->phpInfo();
  $output = $phpinfo[0];
  if (!empty($output)) {
    // Transform output to array.
    $result = pai_list_extract_allowed_values($output, 'list_text', '\=\>');
  }

  return $result;
}

/**
 * Restart apache server.
 */
function pai_common_restart_apache($server_id) {
  $server_wrapper = entity_metadata_wrapper('node', $server_id);
  $field_server_restart_apache = $server_wrapper->field_server_restart_apache->value();
  $field_server_require_root = $server_wrapper->field_server_require_root->value();
  $field_server_user_name = $server_wrapper->field_server_user_name->value();
  $field_server_user_password = field_get_items('node', $server_wrapper->value(), 'field_server_user_password');
  $field_server_user_password = password_field_decrypt($field_server_user_password[0]['password_field']);

  $obj = new PaiApache();
  $obj->command = $field_server_restart_apache;
  $obj->as_root = $field_server_require_root;
  $obj->user = $field_server_user_name;
  $obj->pass = $field_server_user_password;

  return $obj->restartApache();
}

/**
 * Parses a string of 'allowed values' into an array.
 */
function pai_list_extract_allowed_values($string, $field_type, $delimiter = '\|', $generate_keys = FALSE) {
  $values = array();

  $list = explode("\n", $string);
  $list = array_map('trim', $list);
  $list = array_filter($list, 'strlen');

  $generated_keys = $explicit_keys = FALSE;
  foreach ($list as $position => $text) {
    $value = $key = FALSE;

    // Check for an explicit key.
    $matches = array();
    if (preg_match("/(.*)$delimiter(.*)/", $text, $matches)) {
      $key = $matches[1];
      $value = $matches[2];
      $explicit_keys = TRUE;
    }
    // Otherwise see if we can use the value as the key. Detecting true integer
    // strings takes a little trick.
    elseif ($field_type == 'list_text' || ($field_type == 'list_float' && is_numeric($text)) || ($field_type == 'list_integer' && is_numeric($text) && (float) $text == intval($text))) {
      $key = $value = $text;
      $explicit_keys = TRUE;
    }
    // Otherwise see if we can generate a key from the position.
    elseif ($generate_keys) {
      $key = (string) $position;
      $value = $text;
      $generated_keys = TRUE;
    }
    else {
      return;
    }

    // Float keys are represented as strings and need to be disambiguated
    // ('.5' is '0.5').
    if ($field_type == 'list_float' && is_numeric($key)) {
      $key = (string) (float) $key;
    }

    $values[$key] = $value;
  }

  // We generate keys only if the list contains no explicit key at all.
  if ($explicit_keys && $generated_keys) {
    return;
  }

  return $values;
}

/**
 * Download Drupal site using drush alias.
 */
function pai_drush_download_project($site_name, $server_docroot) {
  if ($site_name && is_dir($server_docroot)) {
    $drush_alias_name = '@' . pai_string_to_machinename($site_name);
    $command = "cd $server_docroot && drush $drush_alias_name dl -y;";
    $result = pai_execute_command($command);
    pai_watchdog('pai_common', $result);

    return $result;
  }
}

/**
 * Download Drupal site using drush alias.
 */
function pai_versioning_download_project($site_name, $server_docroot, $command) {
  if ($site_name && is_dir($server_docroot)) {
    $command = "cd $server_docroot && $command $site_name";
    $execute = pai_execute_command($command);
    pai_watchdog('pai_common', $execute);

    return $execute;
  }
}

/**
 * Install Drupal site using drush alias.
 */
function pai_drush_install_project($site_name, $project_docroot, $db_file = NULL) {
  if ($site_name && is_dir($project_docroot)) {
    $drush_alias_name = '@' . pai_string_to_machinename($site_name);
    $command = "cd $project_docroot && drush $drush_alias_name si standard -y";
    $result = pai_execute_command($command);
    pai_watchdog('pai_common site install', $result);

    if (!empty($db_file)) {
      $result = pai_drush_install_database($site_name, $project_docroot, $db_file);
      pai_watchdog('pai_common', $result);
    }

    return $result;
  }
}

/**
 * Install Drupal database using drush alias.
 */
function pai_drush_install_database($site_name, $project_docroot, $db_file) {
  if ($site_name && is_dir($project_docroot)) {
    $drush_alias_name = '@' . pai_string_to_machinename($site_name);

    // Drop database.
    $command = "cd $project_docroot && drush $drush_alias_name sql-drop -y";
    $result = pai_execute_command($command);
    pai_watchdog('pai_common', $result);

    // Install database from file.
    $command = "cd $project_docroot && drush $drush_alias_name sqlc < $db_file";
    $result = pai_execute_command($command);
    pai_watchdog('pai_common', $result);

    return $result;
  }
}

/**
 * Backup Database of Drupal site using drush alias.
 */
function pai_drush_db_backup_project($alias, $project_dump_dir, $user, $password) {
  if (!is_dir($project_dump_dir)) {
    $dir = new PaiFile();
    $dir->directory = $project_dump_dir;
    $dir->user = $user;
    $dir->password = $password;

    $result = $dir->createDirectory();
    pai_watchdog('pai_common', $result);
  }

  if ($alias && is_dir($project_dump_dir)) {
    $backup = new PaiFile();
    $backup->directory = $project_dump_dir;
    $backup->drush_alias = $alias;
    $backup->user = $user;
    $backup->password = $password;

    $result = $backup->backupDatabase();
    pai_watchdog('pai_common', $result);
  }
}

/**
 * Backup Database of Drupal site using drush alias.
 */
function pai_drush_files_backup_project($alias, $project_dump_dir, $user, $password) {
  if (!is_dir($project_dump_dir)) {
    $dir = new PaiFile();
    $dir->directory = $project_dump_dir;
    $dir->user = $user;
    $dir->password = $password;

    $result = $dir->createDirectory();
    pai_watchdog('pai_common', $result);
  }

  if ($alias && is_dir($project_dump_dir)) {
    $backup = new PaiFile();
    $backup->directory = $project_dump_dir;
    $backup->drush_alias = $alias;
    $backup->user = $user;
    $backup->password = $password;

    $result = $backup->backupFiles();
    pai_watchdog('pai_common', $result);
  }
}

/**
 * Get drush aliases.
 */
function pai_drush_get_aliases() {
  $command = "drush sa";
  $result = pai_execute_command($command);
  pai_watchdog('pai_common', $result);
  return $result;
}

/**
 * Compare array key with value.
 */
function pai_common_compare_options($options) {
  foreach ($options as $key => $value) {
    ($key == $value) ? $result['match'][$key] = $value : $result['not_match'][$key] = $value;
  }

  return $result;
}

/**
 * Generate password.
 */
function pai_generate_password($string) {
  if (!empty($string)) {
    $key = md5(drupal_get_hash_salt());
    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
    return $value = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $string, MCRYPT_MODE_ECB, $iv)));
  }
}

/**
 * Write information to watchdog.
 */
function pai_watchdog($type, $variables = array(), $severity = WATCHDOG_NOTICE) {
  if (is_array($variables)) {
    watchdog($type, 'Result: <pre>%message</pre>', array('%message' => print_r($variables, TRUE)), $severity, NULL);
  }
}
