<?php
/**
 * @file
 * Main file for PaiFiles class.
 */

// Include pai_commands.inc file.
module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Class PaiFile
 */
class PaiFile {

  var $directory;
  var $file_name;
  var $file_content;
  var $user;
  var $password;
  var $drush_alias;

  /**
   * Constructor.
   */
  function __construct() {
    $this->directory =
    $this->file_name =
    $this->file_content =
    $this->user =
    $this->password =
    $this->drush_alias = NULL;

  }

  /**
   * Create directory.
   */
  function createDirectory() {
    if (is_writable($this->directory)) {
      // Check if directory is writable.
      $command = "mkdir -p \"$this->directory\"";
    }
    else {
      // Directory is not writable.
      $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'mkdir -p \"$this->directory\"'";
    }

    return pai_execute_command($command);
  }

  /**
   * Remove directory.
   */
  function removeDirectory() {
    $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'chmod 777 $this->directory -R";
    pai_execute_command($command);

    if (is_writable($this->directory)) { // @todo - Remove this condition.
      $command = "rm -r \"$this->directory\"";
    }
    else {
      $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'rm -r \"$this->directory\"'";
    }

    return pai_execute_command($command);
  }

  /**
   * Remove file.
   */
  function removeFile() {
    if (is_writable($this->file_name)) {
      $command = "rm \"$this->file_name\"";
    }
    else {
      $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'rm \"$this->file_name\"'";
    }

    return pai_execute_command($command);
  }

  /**
   * Create file or if file exist append data to it.
   *
   * @see create()
   */
  function addData() {
    $result = NULL;
    // Check if directory all-ready exist.
    if (is_dir($this->directory)) {
      // Check if directory is writable.
      if (is_writable($this->directory) && is_writable($this->file_name)) {
        $command = "echo \"$this->file_content\" >> $this->file_name";
      }
      else {
        //dpm('IN');
        //$command = "sudo -S -u $this->user sh -c 'echo $this->password | sudo -S -u $this->user sh -c 'echo \"$this->file_content\" >> $this->file_name''";
        $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'echo \"$this->file_content\" >> $this->file_name'";
      }
    }
    else {
      // Directory doesn't exist. Create them.
      if ($this->createDirectory()) {
        if (is_writable($this->directory)) {
          $command = "echo \"$this->file_content\" >> $this->file_name";
        }
        else {
          $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'echo \"$this->file_content\" >> $this->file_name'";
        }
      }
    }

    return pai_execute_command($command);
  }

  /**
   * Remove data from file.
   *
   * @see remove()
   */
  function removeData() {
    if (is_writable($this->file_name)) {
      $command = "sed -i \"\|^$this->file_content\$|d\" $this->file_name";
    }
    else {
      $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'sed -i \"\|^$this->file_content\$|d\" $this->file_name'";
    }

    return pai_execute_command($command);
  }

  /**
   * Backup database.
   */
  function backupDatabase() {
    $filename = $this->drush_alias . '_database' . time();
    if (is_writable($this->directory)) {
      //$command = "drush sql-sync @$this->drush_alias default -y";
      $command = "drush @$this->drush_alias sql-dump --gzip --result-file=$this->directory$filename";
    }
    else {
      $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'drush @$this->drush_alias sql-dump --gzip --result-file=$this->directory$filename'";
    }
    return pai_execute_command($command);
  }

  /**
   * Backup files.
   */
  function backupFiles() {
    $filename = $this->drush_alias . '_' . time() . '_files.tar.gz';
    $exclude = '--tar-options="--exclude=.svn --exclude=.idea --exclude=.nbproject --exclude=.git --exclude=sites/default/settings.php"';

    if (is_writable($this->directory)) {
      $command = "drush @$this->drush_alias archive-dump $exclude --destination=$this->directory$filename";
    }
    else {
      $command = "echo $this->password | sudo -S sudo -u $this->user sh -c 'drush @$this->drush_alias archive-dump $exclude --destination=$this->directory$filename'";
    }
    return pai_execute_command($command);
  }
}
