<?php
/**
 * @file
 * Main file for project pull.
 */

// Include pai_commands.inc file.
module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Implements menu callback().
 */
function pai_projects_git_pull_form($form, &$form_state) {
  $form = array();
  $nid = $form_state['build_info']['args'][0];
  if (is_numeric($nid)) {
    $wrappers = pai_manage_hosts_vhosts_wrappers($nid);
    $obj = new PaiGit();
    $obj->docroot = $wrappers->field_project_docroot;
    $pid = $wrappers->project_id;

    drupal_set_title(t('Project @prj git pull', array('@prj' => $wrappers->field_hosts_site_name)));

    $form['submit'] = array('#type' => 'submit', '#value' => t('Git pull'));

    $form['docroot'] = array(
      '#type' => 'value',
      '#value' => $wrappers->field_project_docroot,
    );

    $form['pid'] = array(
      '#type' => 'value',
      '#value' => $pid,
    );
  }
  else {
    drupal_set_message(t('Not found any version control.'), 'warning');
  }

  return $form;
}

/**
 * Implements submit callback().
 */
function pai_projects_git_pull_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['docroot'])) {
    $obj = new PaiGit();
    $obj->docroot = $form_state['values']['docroot'];
    $result = $obj->pull();

    if ($result[2] == 0) {
      drupal_set_message(t('@result', array('@result' => $result[0])));
    }
    else {
      drupal_set_message(t('Please check version control action from console. Git message: "@message"', array('@message' => $result[1])));
    }
  }
}
