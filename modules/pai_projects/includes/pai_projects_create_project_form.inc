<?php
/**
 * @file
 * Main file for pai_projects_list_form()
 */

/**
 * Menu callback.
 */
function pai_projects_list_form($form, &$form_state) {
  drupal_goto('node/add/project-virtual-host');
  return $form;
}
