<?php
/**
 * @file
 * Main file for project version control.
 */

// Include pai_commands.inc file.
module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Implements menu callback().
 */
function pai_projects_version_control_form($form, &$form_state) {
  $form = array();
  $nid = $form_state['build_info']['args'][0];

  if (is_numeric($nid)) {
    $wrappers = pai_manage_hosts_vhosts_wrappers($nid);
    drupal_set_title(t('Project @prj', array('@prj' => $wrappers->field_hosts_site_name)));

    $items['items'] = array(
      l(t('Checkout (Switch branch or tag)'), str_replace('[nid]', $nid, "node/[nid]/checkout")),
      l(t('Logs (Show git history)'), str_replace('[nid]', $nid, "node/[nid]/log")),
      l(t('Pull (Update git commit history)'), str_replace('[nid]', $nid, "node/[nid]/pull")),
    );

    $form['version_control'] = array(
      '#type' => 'item',
      '#title' => t('Version control options'),
      '#markup' => theme('item_list', $items),
    );
  }

  return $form;
}
