<?php
/**
 * @file
 * Main file for pai_projects_list_form()
 */

// Include pai_commands.inc file.
module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Menu callback.
 */
function pai_projects_list_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  $form['information'] = array(
    '#type' => 'item',
    '#title' => t('Information'),
    '#markup' => t('List of the projects who have virtual host.'),
  );

  $form['selected'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#options' => array(
      1 => t('Build selected projects'),
      2 => t('Dump database'),
      3 => t('Dump all files'),
      // @todo - Delete selected projects.
    ),
    '#default_value' => 1,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
  );

  $operations = t('<a href="/node/[nid]/edit">Edit</a> | <a href="/node/[nid]/delete">Delete</a> | <a href="/node/[nid]/version-control">Version control</a>');

  // Append projects table.
  $form += _pai_list_nodes('project_virtual_host', $operations);

  if (!empty($form_state['values']['selected'])) {
    $selected = $form_state['values']['selected'];

    if ($selected == 1) {
      // Build projects.
      return pai_projects_build_confirm($form_state);
    }
    elseif ($selected == 2) {
      // Dump database.
      return pai_projects_db_dump_confirm($form_state);
    }
    elseif ($selected == 3) {
      // Dump files.
      return pai_projects_files_dump_confirm($form_state);
    }
  }

  return $form;
}

/**
 * Submit callback.
 *
 * @see pai_projects_list_form()
 */
function pai_projects_list_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['table'])) {
    $options = pai_common_compare_options($form_state['values']['table']);
    if (!empty($form_state['values']['selected']) && !empty($options['match'])) {
      if (!empty($form_state['values']['selected'])) {
        $form_state['#match'] = $options['match'];
        $form_state['rebuild'] = TRUE;
      }
    }
  }

  if (!empty($form_state['#match'])) {
    $options = $form_state['#match'];
    if ($form_state['values']['submit'] == t('Build')) {
      pai_projects_build_batch_start($options, 'build');
    }
    elseif ($form_state['values']['submit'] == t('Backup database')) {
      pai_projects_build_batch_start($options, 'backup_database');
    }
    elseif ($form_state['values']['submit'] == t('Backup files')) {
      pai_projects_build_batch_start($options, 'backup_files');
    }
  }
}

/**
 * Confirm processing for projects building.
 */
function pai_projects_build_confirm(&$form_state) {
  $nodes = $form_state['#match'];

  $confirm_question = format_plural(count($nodes),
    t('Are you sure you want to build this project?'),
    t('Are you sure you want to build these projects?'));
  // Tell the submit handler to process the form.
  $form['process'] = array('#type' => 'hidden', '#value' => 'true');

  // Make sure the form redirects in the end.
  $form['destination'] = array('#type' => 'hidden', '#value' => $_GET['q']);

  return confirm_form($form, $confirm_question,
    $_GET['q'], t('Press <em>Build</em> to start building process. Or press <em>Cancel</em> to return.'), t('Build'), t('Cancel'));
}

/**
 * Backup database.
 */
function pai_projects_db_dump_confirm(&$form_state) {
  $nodes = $form_state['#match'];

  $confirm_question = format_plural(count($nodes),
    t('Are you sure you want to backup database of this project?'),
    t('Are you sure you want to backup databases of these projects?'));
  // Tell the submit handler to process the form.
  $form['process'] = array('#type' => 'hidden', '#value' => 'true');

  // Make sure the form redirects in the end.
  $form['destination'] = array('#type' => 'hidden', '#value' => $_GET['q']);

  return confirm_form($form, $confirm_question,
    $_GET['q'], t('Press <em>Backup database</em> to start backup process. Or press <em>Cancel</em> to return.'), t('Backup database'), t('Cancel'));
}

/**
 * Backup files.
 */
function pai_projects_files_dump_confirm(&$form_state) {
  $nodes = $form_state['#match'];

  $confirm_question = format_plural(count($nodes),
    t('Are you sure you want to backup files of this project?'),
    t('Are you sure you want to backup files of these projects?'));
  // Tell the submit handler to process the form.
  $form['process'] = array('#type' => 'hidden', '#value' => 'true');

  // Make sure the form redirects in the end.
  $form['destination'] = array('#type' => 'hidden', '#value' => $_GET['q']);

  return confirm_form($form, $confirm_question,
    $_GET['q'], t('Press <em>Backup files</em> to start backup process. Or press <em>Cancel</em> to return.'), t('Backup files'), t('Cancel'));
}

/**
 * Build projects batch operation.
 */
function pai_projects_build_batch_start($options, $op) {
  $operations = array();
  $i = 1;
  foreach ($options as $nid) {
    $node = node_load($nid, NULL, TRUE);
    $wrappers = pai_manage_hosts_vhosts_wrappers($node);
    $field_project_checkout = $wrappers->field_project_checkout;
    if (!empty($field_project_checkout) && $op == 'build') {
      $op = 'checkout';
    }
    $operations[] = array(
      'pai_projects_build_batch_build',
      array(
        $wrappers,
        $op,
        t('(Operation @operation)', array('@operation' => $i)),
      ),
    );
    $i++;
  }

  $batch = array(
    'title' => t('Building'),
    'operations' => $operations,
    'finished' => 'pai_projects_batch_finished',
    'progress_message' => t('Building. Operation @current out of @total.'),
    'error_message' => t('Error!'),
    'file' => drupal_get_path('module', 'pai_projects') . '/includes/pai_projects_list_form.inc',
  );

  batch_set($batch);
}

/**
 * PAI projects building.
 */
function pai_projects_build_batch_build($wrappers, $op, $message, &$context) {
  // Site name.
  $site_name = $wrappers->field_hosts_site_name;
  // Project docroot.
  $project_docroot = $wrappers->field_project_docroot;
  // Server docroot.
  $server_docroot = $wrappers->field_server_docroot;
  // Checkout command.
  $command = $wrappers->field_project_checkout;
  // Drush alias name.
  $alias = pai_string_to_machinename($wrappers->field_hosts_site_name);
  $project_dump_dir = $wrappers->field_project_db_backup;
  $user = $wrappers->field_server_user_name;
  $password = $wrappers->field_server_user_password;
  $db_file = $wrappers->field_project_db_file;

  if ($op == 'build') {
    if (!is_dir($project_docroot)) {
      // Download project only if project folder does not exist.
      pai_drush_download_project($site_name, $server_docroot);
    }
    // Install project.
    pai_drush_install_project($site_name, $project_docroot, $db_file);
  }
  elseif ($op == 'checkout' && !empty($command)) {
    // Checkout project.
    pai_versioning_download_project($site_name, $server_docroot, $command);
    // Install default site.
    pai_drush_install_project($site_name, $project_docroot, $db_file);
  }
  elseif ($op == 'backup_database') {
    // Backup database.
    if (!empty($project_dump_dir)) {
      pai_drush_db_backup_project($alias, $project_dump_dir, $user, $password);
    }
  }
  elseif ($op == 'backup_files') {
    // Backup files.
    if (!empty($project_dump_dir)) {
      pai_drush_files_backup_project($alias, $project_dump_dir, $user, $password);
    }
  }
}

/**
 * Finish of batch.
 */
function pai_projects_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), t('The project was successfully build.'), t('The projects were successfully build.'));
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
  // Providing data for the redirected page is done through $_SESSION.
  foreach ($results as $result) {
    $items[] = t('Building project %title.', array('%title' => $result));
  }

  watchdog('pai_projects', 'Projects building finished.');
}
