<?php
/**
 * @file
 * Main file for project checkout.
 */

// Include pai_commands.inc file.
module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Implements menu callback().
 */
function pai_projects_checkout_form($form, &$form_state) {
  $form = array();
  $nid = $form_state['build_info']['args'][0];
  if (is_numeric($nid)) {
    $wrappers = pai_manage_hosts_vhosts_wrappers($nid);
    $obj = new PaiGit();
    $obj->docroot = $wrappers->field_project_docroot;
    $pid = $wrappers->project_id;

    drupal_set_title(t('Project @prj checkout', array('@prj' => $wrappers->field_hosts_site_name)));

    $form['selected'] = array(
      '#type' => 'select',
      '#title' => t('Branches/Tags'),
      '#options' => array(
        t('Branches') => list_extract_allowed_values($obj->getBranches()[0], 'list_text', FALSE),
        t('Tags') => list_extract_allowed_values($obj->getTags()[0], 'list_text', FALSE),
      ),
      '#default_value' => !empty($_SESSION['#point'][$pid]['point']) ? $_SESSION['#point'][$pid]['point'] : NULL,
      '#description' => t('Chose branch or tag to checkout.'),
    );

    $form['docroot'] = array(
      '#type' => 'value',
      '#value' => $wrappers->field_project_docroot,
    );

    $form['pid'] = array(
      '#type' => 'value',
      '#value' => $pid,
    );

    $form['submit'] = array('#type' => 'submit', '#value' => t('Switch'));
  }
  else {
    drupal_set_message(t('Not found any version control.'), 'warning');
  }

  return $form;
}

/**
 * Implements submit callback().
 */
function pai_projects_checkout_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['selected'])) {
    $point = $form_state['values']['selected'];
    $docroot = $form_state['values']['docroot'];
    $pid = $form_state['values']['pid'];

    $obj = new PaiGit();
    $obj->docroot = $docroot;
    $obj->point = $point;
    $result = $obj->checkout();

    if ($result[2] == 0) {
      $_SESSION['#point'][$pid]['point'] = $point;
      drupal_set_message(t($result[1]));
    }
    else {
      drupal_set_message(t('Please check version control action from console. Git message: "@message"', array('@message' => $result[1])));
    }
  }
}
