<?php
/**
 * @file
 * Main file for pai_servers_restart_apache form.
 */

/**
 * Restart apache server.
 */
function pai_servers_restart_server_form($form, &$form_state) {
  if (!empty($form_state['build_info']['args'][0])) {
    $server_wrapper = entity_metadata_wrapper('node', $form_state['build_info']['args'][0]);

    $form['submit'] = array('#type' => 'submit', '#value' => t('Restart'));

    $form['path'] = array(
      '#type' => 'value',
      '#value' => $server_wrapper->field_server_lamp_path->value(),
    );

    $form['command'] = array(
      '#type' => 'value',
      '#value' => $server_wrapper->field_server_restart_apache->value(),
    );

    $form['user'] = array(
      '#type' => 'value',
      '#value' => $server_wrapper->field_server_user_name->value(),
    );

    $form['as_root'] = array(
      '#type' => 'value',
      '#value' => $server_wrapper->field_server_require_root->value(),
    );
    $field_server_user_password = field_get_items('node', $server_wrapper->value(), 'field_server_user_password');
    $form['password'] = array(
      '#type' => 'value',
      '#value' => password_field_decrypt($field_server_user_password[0]['password_field']),
    );
  }
  else {
    drupal_set_message(t('Wrong server ID in URL/callback.'), 'error');
  }

  return $form;
}

/**
 * Submit callback.
 *
 * @see pai_servers_restart_server_form()
 */
function pai_servers_restart_server_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['command'])) {
    $apache = new PaiApache();
    $apache->path = $form_state['values']['path'];
    $apache->command = $form_state['values']['command'];
    $apache->as_root = $form_state['values']['as_root'];
    $apache->user = $form_state['values']['user'];
    $apache->password = $form_state['values']['password'];
    $apache->restartApache();
  }
}
