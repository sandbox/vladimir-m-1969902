<?php
/**
 * @file
 * Main file for pai_servers_list_form()
 */


module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Menu callback.
 */
function pai_servers_list_form($form, &$form_state) {
  // Custom operation for server.
  $operation = t('<a href="/node/[nid]/edit">Edit</a>  <a href="/node/[nid]/delete">Delete</a>  <a href="/admin/config/pai/server/[nid]/restart">Restart</a>');
  $form += _pai_list_nodes('server', $operation);

  return $form;
}
