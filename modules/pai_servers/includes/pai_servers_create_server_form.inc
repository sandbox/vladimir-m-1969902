<?php
/**
 * @file
 * Main file for pai_servers_create_server_form()
 */

/**
 * Menu callback.
 *
 * @see pai_servers_menu()
 */
function pai_servers_create_server_form($form, &$form_state) {
  drupal_goto('node/add/server');
}
