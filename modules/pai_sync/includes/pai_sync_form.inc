<?php
/**
 * @file
 * Main file for pai_sync_form.
 */

/**
 * Load common functions.
 */
module_load_include('inc', 'pai', '/includes/pai_common');

/**
 * Implements menu callback.
 */
function pai_sync_form() {
  $form = array();
  $pai_drush_get_aliases = pai_drush_get_aliases();
  if ($pai_drush_get_aliases[2] == 0) {
    $aliases = list_extract_allowed_values($pai_drush_get_aliases[0], 'list_text', FALSE);

    $exclude_aliases = array('default', '@none', '@self');
    foreach ($aliases as $key => $alias) {
      if (in_array($alias, $exclude_aliases)) {
        unset($aliases[$key]);
      }
    }
  }
  else {
    drupal_set_message('There are NO aliases found.');
    return;
  }

  // From env.
  $form['from'] = array(
    '#type' => 'select',
    '#title' => t('From'),
    '#options' => $aliases,
    '#description' => t('Select environment from.'),
  );
  // To env.
  $form['to'] = array(
    '#type' => 'select',
    '#title' => t('To'),
    '#options' => $aliases,
    '#description' => t('Select environment to.'),
  );

  $actions = array(
    1 => t('Sync database'),
    2 => t('Sync files'),
  );

  // Actions.
  $form['action'] = array(
    '#type' => 'select',
    '#title' => t('Action'),
    '#options' => $actions,
    '#description' => t('Select synchronisations action.'),
  );

  // Execute.
  $form['submit'] = array('#type' => 'submit', '#value' => t('Synchronizes'));
  return $form;
}

/**
 * Implements submit callback.
 */
function pai_sync_form_submit($form, &$form_state) {
  $from = $form_state['values']['from'];
  $to = $form_state['values']['to'];
  $action = $form_state['values']['action'];

  if ($from != $to) {
    if ($action == 1) {
      // Sync databases.
      $command = "drush sql-sync $from $to -y";
    }
    else {
      // Sync files.
      $command = "drush --yes rsync $from:%files/ $to:%files";
    }

    $res = pai_execute_command($command);
    if ($res[2] == 0) {
      drupal_set_message(t('Synchronisations executed successfully.'));
    }
    else {
      drupal_set_message(t('Console message: "@message"', array('@message' => $res[1])));
    }
  }
  else {
    drupal_set_message(t('Please chose different environments'));
  }
}
