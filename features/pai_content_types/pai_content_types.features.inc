<?php
/**
 * @file
 * pai_content_types.features.inc
 */

/**
 * Implements hook_node_info().
 */
function pai_content_types_node_info() {
  $items = array(
    'drush_alias_model' => array(
      'name' => t('Drush alias model'),
      'base' => 'node_content',
      'description' => t('Drush alias model content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'hosts' => array(
      'name' => t('Host name'),
      'base' => 'node_content',
      'description' => t('PAI hosts content type. Define project (site) host name.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'new_project' => array(
      'name' => t('New project'),
      'base' => 'node_content',
      'description' => t('New project content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'project' => array(
      'name' => t('Existing project'),
      'base' => 'node_content',
      'description' => t('PAI project content type. Add existing project with database.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'project_aliases' => array(
      'name' => t('Project alias'),
      'base' => 'node_content',
      'description' => t('Drush project alias content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'project_virtual_host' => array(
      'name' => t('Project virtual host'),
      'base' => 'node_content',
      'description' => t('Project virtual host content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'server' => array(
      'name' => t('Server'),
      'base' => 'node_content',
      'description' => t('PAI Server content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'virtual_host_model' => array(
      'name' => t('Virtual host model'),
      'base' => 'node_content',
      'description' => t('Virtual host model content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
